/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package runnableprac;

import java.util.Date;
import java.util.Stack;

/**
 *
 * @author Jose
 */

public class RunnablePrac implements Runnable{
    
        public Stack<Integer> numbers = new Stack<Integer>();
        
        public RunnablePrac(Stack<Integer> numbers){
            this.numbers = numbers;
        }
    
        
        public static void main(String[] args) {
            Stack<Integer> numbers = new Stack<Integer>();
                //Rellenamos pila
                for (int i=0; i < 100 ; i++){
                   numbers.push(i);
                  }
        
                    Runnable r1 = new RunnablePrac(numbers);
                    Runnable r2 = new RunnablePrac(numbers);
                    Runnable r3 = new RunnablePrac(numbers);
                    Runnable r4 = new RunnablePrac(numbers);
                    Runnable r5 = new RunnablePrac(numbers);

        new Thread(r1).start();
        new Thread(r2).start();
        new Thread(r3).start();
        new Thread(r4).start();
        new Thread(r5).start();
          
    }
    
    @Override
    public void run() {
         long tiempoInicial = new Date().getTime();
        while (!numbers.isEmpty()){
            Integer number = numbers.pop();
           
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Hemos procesado numero " + number);
        }
        System.out.println("Ha tardado " + (new Date().getTime() - tiempoInicial)/1000 + " segundos"); 
    }
    
}

    
    


package jerarquiadeclases;


public class Persona {
    protected String nom ;
    protected String adresa;
    
    
    public Persona(String nombre,String direc){
        adresa=direc;
        nom=nombre;
    }
    
    public Persona(Persona p){
        this (p.adresa,p.nom);
    }
    
   
    
    public void setNom (String nouNom)  {
      nom = nouNom;
   }
    
    public String getNom ()  {
      return nom;
   }
    
    public void setAdresa(String novaAdresa){
        adresa = novaAdresa;
    }
    
    public String getAdresa(){
        return adresa;
    }
}

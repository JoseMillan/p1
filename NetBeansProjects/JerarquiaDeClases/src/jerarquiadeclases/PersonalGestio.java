
package jerarquiadeclases;


public class PersonalGestio extends Personal{
           private String posicion;
           
    public PersonalGestio(String centre, String nombre, String direc, String numeSS,String posicio) {
        super(nombre,direc,numeSS);
        posicion=posicio;
    }
       public void setPosicio(String pos){
           posicion = pos;
       }
       
       public String getPosicio(){
           return posicion;
       }
    
           
}

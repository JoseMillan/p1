/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package students;

/**
 *
 * @author Jose
 */
public interface StudentDAO  {

   Student getStudent();
   void removeStudent();
   void addStudent();
   void contractStudent();
    
}
